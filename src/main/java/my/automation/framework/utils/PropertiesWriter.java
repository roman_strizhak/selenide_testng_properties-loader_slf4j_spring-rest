package my.automation.framework.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import my.automation.framework.core.logging.Logger;

public final class PropertiesWriter {

    private PropertiesWriter() {
    }

    public static void writePropertiesToFile(final Map<String, String> propertiesMap, final String fileName) {
        final Properties properties = new Properties();
        final String comment = null;
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream("src/main/resources/".concat(fileName));
            propertiesMap.entrySet().forEach(entry -> {
                properties.setProperty(entry.getKey(), entry.getValue());
            });
            properties.store(fileOut, comment);
        } catch (final IOException io) {
            Logger.out.error(io);
        } finally {
            Optional.ofNullable(fileOut).ifPresent(output -> {
                try {
                    output.close();
                } catch (final IOException e) {
                    Logger.out.error(e);
                }
            });
        }
    }
}