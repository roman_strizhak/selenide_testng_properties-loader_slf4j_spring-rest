package my.automation.framework.utils;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import my.automation.framework.core.logging.Logger;

public final class JsonUtil {

    private static final int TAB_WIDTH = 4;
    private static final String EQUALS = "\\u003d";

    private static Gson gsonWithNulls = new GsonBuilder().serializeNulls().create();
    private static Gson gson = new GsonBuilder().create();
    private static String eol = System.getProperty("line.separator");

    private JsonUtil() {
    }

    public static String toJson(final Object obj) {
        return toJson(obj, true);
    }

    public static String toJson(final Object obj, final boolean serializeNulls) {
        final String json;
        if (serializeNulls) {
            json = gsonWithNulls.toJson(obj);
        } else {
            json = gson.toJson(obj);
        }
        /*
         * Escaping equals sign is not necessary
         */
        return json.replace(EQUALS, "=");
    }

    public static String prettyPrintJson(final String json) {
        String result = json;
        try {
            if (json.startsWith("[")) {
                result = eol + new JSONArray(json).toString(TAB_WIDTH);
            } else {
                result = eol + new JSONObject(json).toString(TAB_WIDTH);
            }
        } catch (JSONException e) {
            Logger.out.warn("Could not pretty-print JSON string", e);
        }
        return result;
    }

    public static <T> T fromJson(final String json, final Class<T> type) {
        return gson.fromJson(json, type);
    }

    public static <T> T fromJson(final String json, final Type type) {
        return gson.fromJson(json, type);
    }

    public static String prettyPrintJson(final Object obj) {
        return prettyPrintJson(toJson(obj));
    }
}