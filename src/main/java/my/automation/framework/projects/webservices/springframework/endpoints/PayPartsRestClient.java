package my.automation.framework.projects.webservices.springframework.endpoints;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

import my.automation.framework.core.webservices.springframework.AbstractWebservice;
import my.automation.framework.projects.webservices.springframework.data.bodyobjects.PayPartsLimitBody;
import my.automation.framework.projects.webservices.springframework.data.jsonobjects.PayPartsLimitDTO;
import my.automation.framework.utils.JsonUtil;

public class PayPartsRestClient extends AbstractWebservice {

    private static final String PAY_PARTS_BASE_URL = "https://paypartslimit.privatbank.ua/";
    private static final String PAY_PARTS_LIMIT = "pp-limit/limit";

    public PayPartsLimitDTO getPayPartsLimitState(final PayPartsLimitBody payPartsLimitBody) {
        final HttpEntity<String> response = sendAndGetResponse(PAY_PARTS_BASE_URL + PAY_PARTS_LIMIT, POST,
                getHttpEntity(getHeadersWithJson(), payPartsLimitBody), HttpStatus.OK);
        return JsonUtil.fromJson(response.getBody(), PayPartsLimitDTO.class);
    }
}