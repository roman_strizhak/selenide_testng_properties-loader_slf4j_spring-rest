package my.automation.framework.projects.webservices.springframework.data.bodyobjects;

import com.google.gson.annotations.SerializedName;

import my.automation.framework.core.webservices.springframework.AbstractDTO;

public class PayPartsLimitBody extends AbstractDTO {

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("source")
    private String source;

    public PayPartsLimitBody(final String phone, final String email, final String source) {
        this.phone = phone;
        this.email = email;
        this.source = source;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getSource() {
        return source;
    }
}