package my.automation.framework.projects.webservices.springframework.endpoints;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

import my.automation.framework.core.webservices.springframework.AbstractWebservice;
import my.automation.framework.projects.webservices.springframework.data.jsonobjects.ExchangeDTO;
import my.automation.framework.utils.JsonUtil;

public class ExchangeRateRestClient extends AbstractWebservice {

    private static final String EXCHANGE = "exchange";
    private static final String API_TYPE = "pubinfo";

    public List<ExchangeDTO> getExchangeRates(final int courseIdValue) {
        final HttpEntity<String> response = sendAndGetResponse(
                getBaseUrl(API_TYPE, getParameters(JSON, EXCHANGE, "coursid=".concat(String.valueOf(courseIdValue)))),
                GET, getHttpEntity(getHeadersWithJson()), HttpStatus.OK);
        final Type type = new TypeToken<ArrayList<ExchangeDTO>>() {
        }.getType();
        return JsonUtil.fromJson(response.getBody(), type);
    }
}