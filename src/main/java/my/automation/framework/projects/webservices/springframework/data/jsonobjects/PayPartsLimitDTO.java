package my.automation.framework.projects.webservices.springframework.data.jsonobjects;

import com.google.gson.annotations.SerializedName;

import my.automation.framework.core.webservices.springframework.AbstractDTO;

public class PayPartsLimitDTO extends AbstractDTO {

    @SerializedName("state")
    private String state;

    @SerializedName("message")
    private String message;

    public String getState() {
        return state;
    }

    public String getMessage() {
        return message;
    }
}