package my.automation.framework.projects.webservices.springframework.data.jsonobjects;

import com.google.gson.annotations.SerializedName;

import my.automation.framework.core.webservices.springframework.AbstractDTO;

public class ExchangeDTO extends AbstractDTO {

    @SerializedName("base_ccy")
    private String baseCcy;

    @SerializedName("buy")
    private String buy;

    @SerializedName("ccy")
    private String ccy;

    @SerializedName("sale")
    private String sale;

    public String getBaseCcy() {
        return baseCcy;
    }

    public String getBuy() {
        return buy;
    }

    public String getCcy() {
        return ccy;
    }

    public String getSale() {
        return sale;
    }
}