package my.automation.framework.projects.gamegabrielecirulli.containers;

import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;

import org.openqa.selenium.support.FindBy;

public class GameContainer extends ElementsContainer {

    @FindBy(xpath = ".//*[@class='grid-container']")
    private SelenideElement gridContainer;

    @FindBy(xpath = ".//*[@class='tile-container']")
    private SelenideElement tileContainer;

    @FindBy(xpath = ".//*[contains(@class,'game-message')]/p")
    private SelenideElement gameMessage;

    public SelenideElement getGameContainer() {
        return gridContainer;
    }

    public SelenideElement getTileContainer() {
        return tileContainer;
    }

    public SelenideElement getGameMessage() {
        return gameMessage;
    }
}