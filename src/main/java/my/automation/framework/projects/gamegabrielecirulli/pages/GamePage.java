package my.automation.framework.projects.gamegabrielecirulli.pages;

import org.openqa.selenium.support.FindBy;

import my.automation.framework.core.AbstractPageObject;
import my.automation.framework.projects.gamegabrielecirulli.containers.AboveGameContainer;
import my.automation.framework.projects.gamegabrielecirulli.containers.GameContainer;
import my.automation.framework.projects.gamegabrielecirulli.containers.HeaderContainer;

public class GamePage extends AbstractPageObject {

    @FindBy(css = "div.heading")
    private HeaderContainer headerContainer;

    @FindBy(css = "div.above-game")
    private AboveGameContainer aboveGameContainer;

    @FindBy(css = "div.game-container")
    private GameContainer gameContainer;

    public GameContainer getGameContainer() {
        return gameContainer;
    }

    public HeaderContainer getHeaderContainer() {
        return headerContainer;
    }

    public AboveGameContainer getAboveGameContainer() {
        return aboveGameContainer;
    }
}