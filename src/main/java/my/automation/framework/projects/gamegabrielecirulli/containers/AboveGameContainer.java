package my.automation.framework.projects.gamegabrielecirulli.containers;

import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;

import org.openqa.selenium.support.FindBy;

public class AboveGameContainer extends ElementsContainer {

    @FindBy(xpath = ".//*[@class='game-intro']")
    private SelenideElement gameIntroLabel;

    @FindBy(xpath = ".//*[@class='restart-button']")
    private SelenideElement newGameButton;

    public SelenideElement gameGameIntroLabel() {
        return gameIntroLabel;
    }

    public SelenideElement getNewGameButton() {
        return newGameButton;
    }
}