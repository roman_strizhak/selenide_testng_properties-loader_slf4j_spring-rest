package my.automation.framework.projects.gamegabrielecirulli.containers;

import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;

import org.openqa.selenium.support.FindBy;

public class HeaderContainer extends ElementsContainer {

    @FindBy(xpath = ".//*[@class='title']")
    private SelenideElement titleLabel;

    @FindBy(xpath = ".//*[@class='score-container']")
    private SelenideElement scoreBox;

    @FindBy(xpath = ".//*[@class='best-container']")
    private SelenideElement bestBox;

    public SelenideElement getTitleLabel() {
        return titleLabel;
    }

    public SelenideElement getScoreBox() {
        return scoreBox;
    }

    public SelenideElement getBestBox() {
        return bestBox;
    }
}