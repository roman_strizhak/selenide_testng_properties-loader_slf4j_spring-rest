package my.automation.framework.projects.gamegabrielecirulli.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public final class GameUtil {

    private GameUtil() {
    }

    public static String getRandomArrowKey() {
        final List<String> arrowKeys = Arrays.asList("UP", "DOWN", "LEFT", "RIGHT");
        return arrowKeys.get(new Random().nextInt(arrowKeys.size()));
    }
}