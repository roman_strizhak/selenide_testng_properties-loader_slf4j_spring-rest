package my.automation.framework.core.common;

import my.automation.framework.core.logging.Logger;
import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

@Resource.Classpath("framework.properties")
public final class FrameworkProperties {

    private static FrameworkProperties frameworkProperties;

    @Property("driver.browser")
    private String browser;

    @Property("driver.timeout")
    private long timeout;

    public static FrameworkProperties getProperties() {
        if (frameworkProperties == null) {
            frameworkProperties = new FrameworkProperties();
            Logger.out.info("Reading framework properties: %s file",
                    FrameworkProperties.class.getAnnotation(Resource.Classpath.class).value());
            PropertyLoader.populate(frameworkProperties);
        }
        return frameworkProperties;
    }

    String getBrowser() {
        return browser;
    }

    public long getTimeout() {
        return timeout;
    }
}