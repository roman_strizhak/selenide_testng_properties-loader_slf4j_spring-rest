package my.automation.framework.core.common.testsession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import my.automation.framework.core.logging.Logger;

public final class SessionController {

    private static final ThreadLocal<SessionMapImpl> TEST_SESSION_THREAD_LOCAL = new ThreadLocal<>();
    private static final String CASTING_ERROR_MESSAGE = "Could not cast value stored in session with key [%s] to %s";

    private SessionController() {
    }

    @SuppressWarnings("unchecked")
    private static SessionMap<Object, Object> getCurrentSession() {
        if (TEST_SESSION_THREAD_LOCAL.get() == null) {
            TEST_SESSION_THREAD_LOCAL.set(new SessionMapImpl());
        }
        return TEST_SESSION_THREAD_LOCAL.get();
    }

    public static void storeObjectInSession(final String key, final Object obj) {
        getCurrentSession().put(key, obj);
        Logger.out.debug("Key '%s' = '%s' has been saved in Test session", key, obj.toString());
    }

    public static <T> T getObjectFromSession(final String key, final Class<T> returnType) {
        final Object obj = getCurrentSession().get(key);
        T result = null;
        try {
            result = returnType.cast(obj);
        } catch (final ClassCastException e) {
            final String message = String.format(CASTING_ERROR_MESSAGE, key, returnType.getName());
            Logger.out.warn(message, e);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getListOfObjectsFromSession(final String key, final Class<T> returnType) {
        final Object obj = getCurrentSession().get(key);
        List<T> result = new ArrayList<T>();
        try {
            result = (List<T>) obj;
        } catch (final ClassCastException e) {
            final String message = String.format(CASTING_ERROR_MESSAGE, key, returnType.getName());
            Logger.out.warn(message, e);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> Map<String, T> getMapFromSession(final String key, final Class<T> returnType) {
        final Object obj = getCurrentSession().get(key);
        Map<String, T> result = new HashMap<String, T>();
        try {
            result = (Map<String, T>) obj;
        } catch (final ClassCastException e) {
            final String message = String.format(CASTING_ERROR_MESSAGE, key, returnType.getName());
            Logger.out.warn(message, e);
        }
        return result;
    }

    public static String getStringFromSession(final String key) {
        return getObjectFromSession(key, String.class);
    }

    public static Integer getIntegerFromSession(final String key) {
        return getObjectFromSession(key, Integer.class);
    }

    public static Long getLongFromSession(final String key) {
        return getObjectFromSession(key, Long.class);
    }

    public static Double getDoubleFromSession(final String key) {
        return getObjectFromSession(key, Double.class);
    }

    public static List<String> getListOfStringsFromSession(final String key) {
        return getListOfObjectsFromSession(key, String.class);
    }

    public static boolean isKeyPresentInSession(final String key) {
        return getCurrentSession().containsKey(key);
    }

    public static void removeObjectFromSession(final String key) {
        if (isKeyPresentInSession(key)) {
            getCurrentSession().remove(key);
        }
    }

    public static void clearTestSession() {
        getCurrentSession().clear();
    }

    public static void clearTestSessionVariables() {
        getCurrentSession().keySet().forEach(key -> getCurrentSession().remove(key));
    }
}