package my.automation.framework.core.common.project.properties;

public final class PropertyNames {

    public static final String PROJECT_CONFIG_FILE = "project.config.file";

    public static final String BASE_URL = "base.url";

    private PropertyNames() {
    }

}