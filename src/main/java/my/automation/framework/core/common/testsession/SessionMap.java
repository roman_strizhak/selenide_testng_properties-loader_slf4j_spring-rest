package my.automation.framework.core.common.testsession;

import java.util.Map;

interface SessionMap<K, V> extends Map<K, V> {

    Map<String, String> getMetaData();

    void addMetaData(String key, String value);

    void clearMetaData();

    void shouldContainKey(K key);
}