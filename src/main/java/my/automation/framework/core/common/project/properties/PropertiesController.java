package my.automation.framework.core.common.project.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import my.automation.framework.core.logging.Logger;

import static java.util.Optional.ofNullable;

public final class PropertiesController {

    private static final String PROPERTIES_DIRECTORY_PATH = "/project/properties/";
    private static PropertiesController propertiesController;

    private final Properties properties = new Properties();

    private PropertiesController() {
        try {
            final String propertyFile = ofNullable(System.getProperty(PropertyNames.PROJECT_CONFIG_FILE)).orElseThrow(
                    () -> new NullPointerException("'-Dproject.config.file={name}.properties' NOT SET for run tests!"));
            loadProperties(propertyFile);
        } catch (final IOException e) {
            throw new IllegalStateException("Failed to load project configuration file", e);
        }
    }

    public static String getProperty(final String propertyName) {
        propertiesController = ofNullable(propertiesController).orElseGet(PropertiesController::new);
        return System.getProperty(propertyName, propertiesController.properties.getProperty(propertyName));
    }

    private void loadProperties(final String propertyFile) throws IOException {
        Logger.out.info("Reading project properties: %s file", propertyFile);
        final InputStream inputStream = getClass().getResourceAsStream(PROPERTIES_DIRECTORY_PATH.concat(propertyFile));
        ofNullable(inputStream)
                .orElseThrow(() -> new IOException("Unable to open stream for resource ".concat(propertyFile)));
        final Properties props = new Properties();
        props.load(inputStream);
        inputStream.close();
        for (final String propertyName : props.stringPropertyNames()) {
            if (propertyName.startsWith("+")) {
                loadProperties(propertyName.substring(1));
            }
        }
        properties.putAll(props);
    }
}