package my.automation.framework.core.common;

import java.util.LinkedHashMap;
import java.util.Map;

import com.codeborne.selenide.Configuration;

import my.automation.framework.core.common.project.properties.PropertiesController;
import my.automation.framework.core.common.project.properties.PropertyNames;

public final class SelenideConfig {

    private SelenideConfig() {
    }

    public static void applyConfiguration() {
        Configuration.browser = FrameworkProperties.getProperties().getBrowser();
        Configuration.timeout = FrameworkProperties.getProperties().getTimeout();
        Configuration.baseUrl = PropertiesController.getProperty(PropertyNames.BASE_URL);
    }

    public static Map<String, String> getConfiguration() {
        final Map<String, String> configuration = new LinkedHashMap<>();
        configuration.put("browser", Configuration.browser);
        configuration.put("project.config.file", System.getProperty(PropertyNames.PROJECT_CONFIG_FILE));
        configuration.put("base.url", Configuration.baseUrl);
        return configuration;
    }
}