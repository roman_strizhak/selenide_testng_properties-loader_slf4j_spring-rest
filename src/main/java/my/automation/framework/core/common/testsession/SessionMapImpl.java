package my.automation.framework.core.common.testsession;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.ImmutableMap;

public class SessionMapImpl<K, V> extends ConcurrentHashMap implements SessionMap {

    private final Map<String, String> metadata = new ConcurrentHashMap();

    @Override
    public void shouldContainKey(final Object key) {
        Optional.ofNullable(super.get(key)).orElseThrow(
                () -> new AssertionError(String.format("Session variable %s expected, but not found.", key)));
    }

    @Override
    public Object put(final Object key, final Object value) {
        if (value == null) {
            return remove(key);
        } else {
            return super.put(key, value);
        }
    }

    @Override
    public Map<String, String> getMetaData() {
        return ImmutableMap.copyOf(metadata);
    }

    @Override
    public void addMetaData(final String key, final String value) {
        metadata.put(key, value);
    }

    @Override
    public void clearMetaData() {
        metadata.clear();
    }

    @Override
    public void clear() {
        clearMetaData();
        super.clear();
    }
}