package my.automation.framework.core.common.testsession;

public final class SessionVariables {

    public static final String GAME_RESULT_MESSAGE = "game.result.message";

    private SessionVariables() {
    }
}