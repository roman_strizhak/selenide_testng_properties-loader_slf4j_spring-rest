package my.automation.framework.core;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.WebDriverRunner.source;
import static com.codeborne.selenide.WebDriverRunner.url;

public abstract class AbstractPageObject extends Selenide {

    public String getTitle() {
        return title();
    }

    public String getUrl() {
        return url();
    }

    public String getSource() {
        return source();
    }

    public void scrollToElement(final SelenideElement element) {
        final String script = "arguments[0].scrollIntoView(false);";
        executeJavaScript(script, element);
    }
}