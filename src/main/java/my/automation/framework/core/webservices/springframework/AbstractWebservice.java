package my.automation.framework.core.webservices.springframework;

import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import my.automation.framework.core.logging.Logger;
import my.automation.framework.utils.JsonUtil;

import static my.automation.framework.core.reporting.Attachments.attachDataToReport;

public abstract class AbstractWebservice {

    protected static final HttpMethod GET = HttpMethod.GET;
    protected static final HttpMethod POST = HttpMethod.POST;
    protected static final HttpMethod PUT = HttpMethod.PUT;
    protected static final HttpMethod DELETE = HttpMethod.DELETE;
    protected static final HttpMethod PATCH = HttpMethod.PATCH;
    protected static final String JSON = "json";
    private static final String BASE_PATH = "p24api/";
    private static final String NEW_LINE = "\n";

    private static RestTemplate restTemplate;
    private static HttpHeaders headers = new HttpHeaders();

    protected AbstractWebservice() {
    }

    private static RestTemplate getRestTemplate() {
        if (null == restTemplate) {
            restTemplate = new RestTemplate();
            restTemplate.setRequestFactory(createHttpClientFactory());
        }
        return restTemplate;
    }

    private static ClientHttpRequestFactory createHttpClientFactory() {
        int timeout = 60 * 1000;
        final HttpComponentsClientHttpRequestFactory clientFactory = new HttpComponentsClientHttpRequestFactory(
                DefaultSecureHttpClient.getSecureClient());
        clientFactory.setConnectionRequestTimeout(timeout);
        clientFactory.setReadTimeout(timeout);
        clientFactory.setConnectTimeout(timeout);
        return clientFactory;
    }

    protected static HttpEntity<String> sendAndGetResponse(final String uri, final HttpMethod httpMethod,
            final HttpEntity<?> entity, final HttpStatus code) {
        return sendAndGetResponse(getUriFromString(uri), httpMethod, entity, code);
    }

    private static HttpEntity<String> sendAndGetResponse(final URI uri, final HttpMethod httpMethod,
            final HttpEntity<?> entity, final HttpStatus code) {
        HttpEntity<String> response;
        try {
            response = send(uri, httpMethod, entity);
            final StringBuilder responseLog = new StringBuilder().append(((ResponseEntity) response).getStatusCode())
                    .append(" ").append(uri).append(NEW_LINE).append("RESPONSE BODY: ").append(response.getBody());
            attachDataToReport(responseLog);
        } catch (final HttpClientErrorException | HttpServerErrorException e) {
            final StringBuilder statusLog = new StringBuilder(e.getStatusText()).append(NEW_LINE);
            statusLog.append("URL: ").append(uri).append(NEW_LINE);
            statusLog.append(e.getResponseBodyAsString());
            Logger.out.error(e);
            throw new HttpClientErrorException(e.getStatusCode(), statusLog.toString());
        }
        return Optional.of(response).filter(resp -> code.equals(((ResponseEntity) resp).getStatusCode()))
                .orElseThrow(() -> new AssertionError("Wrong status code! - ".concat(code.toString())));
    }

    protected static HttpStatus getStatusCodeFromResponse(final String uri, final HttpMethod httpMethod,
            final HttpEntity<?> entity) {
        HttpStatus response;
        try {
            response = ((ResponseEntity) send(getUriFromString(uri), httpMethod, entity)).getStatusCode();
        } catch (final HttpServerErrorException | HttpClientErrorException e) {
            Logger.out.error(e);
            response = e.getStatusCode();
        }
        return response;
    }

    private static HttpEntity<String> send(final URI uri, final HttpMethod httpMethod, final HttpEntity<?> entity) {
        StringBuilder request = new StringBuilder().append(httpMethod).append(" ").append(uri).append(NEW_LINE)
                .append("REQUEST BODY: ").append(entity.getBody());
        attachDataToReport(request);
        return getRestTemplate().exchange(uri, httpMethod, entity, String.class);
    }

    protected static void addHeader(final String headerName, final String headerValue) {
        headers.set(headerName, headerValue);
    }

    protected static void clearHeaders() {
        headers = new HttpHeaders();
    }

    protected static HttpHeaders getHeadersWithJson() {
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return headers;
    }

    protected static HttpEntity<String> getHttpEntity(final HttpHeaders headers) {
        return new HttpEntity<>(headers);
    }

    protected static HttpEntity<String> getHttpEntity(final String body) {
        return new HttpEntity<>(body);
    }

    protected static HttpEntity<String> getHttpEntity(final HttpHeaders headers, final String body) {
        return new HttpEntity<>(body, headers);
    }

    protected static HttpEntity<String> getHttpEntity(final HttpHeaders headers, final Object body) {
        return new HttpEntity<>(JsonUtil.toJson(body, false), headers);
    }

    protected static String getBaseUrl(final String apiType, final String pathOrParameters) {
        return "https://api.privatbank.ua/" + BASE_PATH + apiType + "?" + pathOrParameters;
    }

    private static URI getUriFromString(final String uri) {
        return UriComponentsBuilder.fromHttpUrl(uri).build().toUri();
    }

    protected static URI getUriFromString(final String uri, final Map<String, String> uriParameters) {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.setAll(uriParameters);
        return UriComponentsBuilder.fromHttpUrl(uri).queryParams(mvm).build().toUri();
    }

    protected String getParameters(final String... parameters) {
        return Stream.of(parameters).collect(Collectors.joining("&"));
    }
}