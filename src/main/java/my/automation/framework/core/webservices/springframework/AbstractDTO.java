package my.automation.framework.core.webservices.springframework;


import my.automation.framework.utils.JsonUtil;

public abstract class AbstractDTO {

    @Override
    public String toString() {
        return JsonUtil.prettyPrintJson(this);
    }
}