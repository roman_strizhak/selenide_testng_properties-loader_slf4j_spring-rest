package my.automation.framework.core.reporting;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import my.automation.framework.core.logging.Logger;
import ru.yandex.qatools.allure.annotations.Attachment;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public final class Attachments {

    private Attachments() {

    }

    @Attachment("Result")
    public static Object attachDataToReport(final Object data) {
        Logger.out.info(data);
        return data;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] saveScreenshot() {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }
}