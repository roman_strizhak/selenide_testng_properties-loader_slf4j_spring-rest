package my.automation.framework.tests.gamegabrielecirulli;

import org.junit.Assert;
import org.testng.annotations.Test;

import my.automation.framework.TestConditions;
import my.automation.framework.steps.gamegabrielecirulli.PlayGameStates;
import ru.yandex.qatools.allure.annotations.Features;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

public class OverviewTest extends TestConditions {

    private PlayGameStates playGameStates = new PlayGameStates();

    @Features("Page overview")
    @Test()
    public void checkPageTitle() {
        Assert.assertThat(playGameStates.getTitle(), is("2048"));
    }

    @Features("Page overview")
    @Test()
    public void pageUrl() {
        assertThat(playGameStates.getUrl(), containsString("/2048/"));
    }
}