package my.automation.framework.tests.gamegabrielecirulli;

import org.testng.annotations.Test;

import my.automation.framework.TestConditions;
import my.automation.framework.core.reporting.Attachments;
import my.automation.framework.projects.gamegabrielecirulli.utils.GameUtil;
import my.automation.framework.steps.gamegabrielecirulli.PlayGameActions;
import my.automation.framework.steps.gamegabrielecirulli.PlayGameStates;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

public class GameTest extends TestConditions {

    private PlayGameActions playGameActions = new PlayGameActions();
    private PlayGameStates playGameStates = new PlayGameStates();

    @Features("Gaming")
    @Stories("Play game")
    @Severity(value = SeverityLevel.CRITICAL)
    @Test(invocationCount = 1)
    public void playGame() {
        while (playGameStates.gameIsContinued()) {
            playGameActions.pushKey(GameUtil.getRandomArrowKey());
            playGameActions.printCurrentGamesField();
        }
        Attachments.attachDataToReport(
                (playGameStates.getGameResultMessage() + " Score: " + playGameStates.getCurrentScore()));
    }

    @Features("Gaming")
    @Test()
    public void scoreValueAfterGameWasFinished() {
        while (playGameStates.gameIsContinued()) {
            playGameActions.pushKey(GameUtil.getRandomArrowKey());
        }
        Attachments.attachDataToReport(
                playGameStates.getGameResultMessage() + " Score: " + playGameStates.getCurrentScore());
        assertThat(Integer.parseInt(playGameStates.getCurrentScore()), not(0));
    }
}