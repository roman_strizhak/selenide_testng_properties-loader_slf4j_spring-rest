package my.automation.framework.tests.webservises.springframework;

import org.testng.annotations.Test;

import my.automation.framework.TestConditions;
import my.automation.framework.steps.webservises.ExchangeRateStates;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class ExchangeRatesTest extends TestConditions {

    private ExchangeRateStates exchangeRateStates = new ExchangeRateStates();

    @Test()
    public void checkRates1() {
        assertThat(exchangeRateStates.getExchangeRates(4).size(), is(14));
    }

    @Test()
    public void checkRates2() {
        assertThat(exchangeRateStates.getExchangeRates(5), not(empty()));
    }
}