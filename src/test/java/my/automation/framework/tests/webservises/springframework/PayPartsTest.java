package my.automation.framework.tests.webservises.springframework;

import org.testng.annotations.Test;

import my.automation.framework.TestConditions;
import my.automation.framework.steps.webservises.PayPartsStates;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.is;

public class PayPartsTest extends TestConditions {

    private static final String EMPTY_STRING = "";

    private PayPartsStates payPartsStates = new PayPartsStates();

    @Test()
    public void checkPayPartLimitState() {
        assertThat(payPartsStates.getPayPartsLimitState("380979999999", "test@test.com", "0"), equalTo("success"));
    }

    @Test()
    public void checkPayPartLimitErrorMessageForEmptyPhone() {
        assertThat(payPartsStates.getPayPartsLimitErrorMessage(EMPTY_STRING, EMPTY_STRING, "1"),
                equalToIgnoringCase("Неверный формат телефона. Ожидается 380123456789"));
    }

    @Test()
    public void checkPayPartLimitErrorMessageForEmptySource() {
        assertThat(payPartsStates.getPayPartsLimitErrorMessage(EMPTY_STRING, EMPTY_STRING, EMPTY_STRING),
                is("Не передан параметр source"));
    }
}