package my.automation.framework;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import my.automation.framework.core.common.SelenideConfig;
import my.automation.framework.core.common.testsession.SessionController;
import my.automation.framework.core.logging.Logger;
import my.automation.framework.utils.PropertiesWriter;

public class TestConditions {

    @BeforeSuite
    public void beforeSuite() {
        Logger.out.info("Preparing launch tests...\n===============================================");
        SelenideConfig.applyConfiguration();
        Logger.out.info("Start running tests...\n===============================================");
    }

    @BeforeMethod
    public void beforeMethod() {
        Selenide.open(Configuration.baseUrl);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        WebDriverRunner.clearBrowserCache();
        SessionController.clearTestSessionVariables();
    }

    @AfterClass
    public void afterClass() {
        SessionController.clearTestSession();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        WebDriverRunner.closeWebDriver();
        PropertiesWriter.writePropertiesToFile(SelenideConfig.getConfiguration(), "environment.properties");
    }
}