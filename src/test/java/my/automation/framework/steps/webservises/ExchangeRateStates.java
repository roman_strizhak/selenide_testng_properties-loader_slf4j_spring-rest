package my.automation.framework.steps.webservises;

import java.util.Set;
import java.util.stream.Collectors;

import my.automation.framework.core.webservices.springframework.AbstractDTO;
import my.automation.framework.projects.webservices.springframework.endpoints.ExchangeRateRestClient;
import ru.yandex.qatools.allure.annotations.Step;

public class ExchangeRateStates {

    private ExchangeRateRestClient exchangeRateClient = new ExchangeRateRestClient();

    @Step
    public Set<String> getExchangeRates(final int id) {
        return exchangeRateClient.getExchangeRates(id).stream().map(AbstractDTO::toString).collect(Collectors.toSet());
    }
}