package my.automation.framework.steps.webservises;

import my.automation.framework.projects.webservices.springframework.data.bodyobjects.PayPartsLimitBody;
import my.automation.framework.projects.webservices.springframework.data.jsonobjects.PayPartsLimitDTO;
import my.automation.framework.projects.webservices.springframework.endpoints.PayPartsRestClient;
import ru.yandex.qatools.allure.annotations.Step;

public class PayPartsStates {

    private PayPartsRestClient payPartsRestClient = new PayPartsRestClient();

    @Step
    public String getPayPartsLimitState(final String phone, final String email, final String source) {
        return getPayPartsLimit(phone, email, source).getState();
    }

    @Step
    public String getPayPartsLimitErrorMessage(final String phone, final String email, final String source) {
        return getPayPartsLimit(phone, email, source).getMessage();
    }

    private PayPartsLimitDTO getPayPartsLimit(final String phone, final String email, final String source) {
        final PayPartsLimitBody payPartsLimitBody = new PayPartsLimitBody(phone, email, source);
        return payPartsRestClient.getPayPartsLimitState(payPartsLimitBody);
    }
}