package my.automation.framework.steps.gamegabrielecirulli;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;

import org.openqa.selenium.Keys;

import my.automation.framework.core.common.FrameworkProperties;
import my.automation.framework.core.logging.Logger;
import my.automation.framework.core.reporting.Attachments;
import my.automation.framework.projects.gamegabrielecirulli.pages.GamePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;

public class PlayGameActions {

    private GamePage gamePage = page(GamePage.class);
    private StringBuilder stringBuilder = new StringBuilder();

    @Step
    public void pushKey(final String keyName) {
        gamePage.getGameContainer().getSelf().sendKeys(Keys.valueOf(keyName));
        Logger.out.info("Push key " + keyName);
    }

    @Step
    public void printCurrentGamesField() {
        Attachments.attachDataToReport(getCurrentGameFieldValues());
    }

    private String getCurrentGameFieldValues() {
        stringBuilder.setLength(0);
        stringBuilder.append("Current game result:\n");
        final String[] arrTilePositions = { "1-1", "2-1", "3-1", "4-1", "1-2", "2-2", "3-2", "4-2", "1-3", "2-3", "3-3",
                "4-3", "1-4", "2-4", "3-4", "4-4" };
        for (String position : arrTilePositions) {
            stringBuilder.append("| ").append(getTileValue(position)).append(" ")
                    .append(position.contains("4-") ? "|\n" : "");
        }
        return stringBuilder.toString();
    }

    private String getTileValue(final String position) {
        String tileValue = "0";
        String tileSelector = "[class*=\"tile-position-" + position + "\"]";
        if (isTileExists(tileSelector)) {
            ElementsCollection tiles = gamePage.getGameContainer().getTileContainer().findAll(tileSelector);
            tileValue = tiles.size() > 1 ? tiles.get(2).text() : tiles.get(0).text();
        }
        return tileValue;
    }

    private boolean isTileExists(final String tileSelector) {
        if ("[class*=\"tile-position-1-1\"]".equals(tileSelector)) {
            sleep(100);
        }
        Configuration.timeout = 1;
        boolean isExists = $(tileSelector).is(exist);
        Configuration.timeout = FrameworkProperties.getProperties().getTimeout();
        return isExists;
    }
}