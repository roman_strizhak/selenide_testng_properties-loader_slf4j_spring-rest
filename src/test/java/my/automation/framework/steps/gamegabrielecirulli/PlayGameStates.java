package my.automation.framework.steps.gamegabrielecirulli;

import java.util.Optional;

import my.automation.framework.core.common.testsession.SessionController;
import my.automation.framework.core.common.testsession.SessionVariables;
import my.automation.framework.projects.gamegabrielecirulli.pages.GamePage;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.page;

public class PlayGameStates {

    private GamePage gamePage = page(GamePage.class);

    public String getUrl() {
        return gamePage.getUrl();
    }

    public String getTitle() {
        return gamePage.getTitle();
    }

    public boolean gameIsContinued() {
        final String gameResultMessage = getGameResultMessage();
        SessionController.storeObjectInSession(SessionVariables.GAME_RESULT_MESSAGE, gameResultMessage);
        return Optional.ofNullable(SessionController.getStringFromSession(SessionVariables.GAME_RESULT_MESSAGE))
                .map(String::isEmpty).orElse(true);
    }

    public String getGameResultMessage() {
        return gamePage.getGameContainer().getGameMessage().text();
    }

    @Step
    public String getCurrentScore() {
        String scoreValue = gamePage.getHeaderContainer().getScoreBox().text();
        return scoreValue.contains("+") ? scoreValue.substring(0, scoreValue.indexOf('+')).trim() : scoreValue.trim();
    }
}