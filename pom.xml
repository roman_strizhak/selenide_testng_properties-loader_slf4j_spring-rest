<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>my.automation.framework</groupId>
    <artifactId>my-automation-framework</artifactId>
    <url>http://gabrielecirulli.github.io/2048/</url>
    <version>1.0</version>

    <name>Automated tests</name>
    <description>Usage: Selenide - Java - Maven - TestNG - Allure</description>

    <scm>
        <url>https://bitbucket.org/roman_strizhak/my-automation-framework</url>
        <connection>git@bitbucket.org:roman_strizhak/my-automation-framework.git</connection>
    </scm>

    <organization>
        <name>EPAM Systems</name>
        <url>https://www.epam.com/</url>
    </organization>

    <developers>
        <developer>
            <name>Roman Strizhak</name>
            <id>4060741400035094259</id>
            <email>roman.strizhak@gmail.com</email>
            <url>https://www.linkedin.com/in/romanstrizhak</url>
            <roles>
                <role>Senior Software Test Automation Engineer</role>
            </roles>
            <organization>EPAM Systems</organization>
            <organizationUrl>https://www.epam.com/</organizationUrl>
        </developer>
    </developers>

    <properties>
        <selenide.version>3.7</selenide.version>
        <yandex.properties.version>1.4</yandex.properties.version>

        <allure.version>1.4.24.RC3</allure.version>
        <aspectj.version>1.8.5</aspectj.version>

        <junit.version>4.11</junit.version>
        <hamcrest.version>1.3</hamcrest.version>

        <slf4j-log4j12.version>1.7.12</slf4j-log4j12.version>

        <spring-web.version>4.2.4.RELEASE</spring-web.version>
        <json.version>20080701</json.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.codeborne</groupId>
            <artifactId>selenide</artifactId>
            <version>${selenide.version}</version>
        </dependency>
        <dependency>
            <groupId>ru.yandex.qatools.properties</groupId>
            <artifactId>properties-loader</artifactId>
            <version>${yandex.properties.version}</version>
        </dependency>
        <dependency>
            <groupId>ru.yandex.qatools.allure</groupId>
            <artifactId>allure-testng-adaptor</artifactId>
            <version>${allure.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>hamcrest-core</artifactId>
                    <groupId>org.hamcrest</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <version>${hamcrest.version}</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j-log4j12.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring-web.version}</version>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>${json.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.11</version>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.11</version>
                <configuration>
                    <includes>
                        <include>**/*Test.java</include>
                        <include>**/Test*.java</include>
                        <include>**/*Test*.java</include>
                    </includes>
                    <testFailureIgnore>true</testFailureIgnore>
                    <argLine>
                        -javaagent:"${settings.localRepository}/org/aspectj/aspectjweaver/${aspectj.version}/aspectjweaver-${aspectj.version}.jar"
                    </argLine>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjweaver</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <phase>pre-site</phase>
                        <configuration>
                            <target>
                                <copy file="${project.basedir}\src\main\resources\environment.properties"
                                      todir="${project.build.directory}\allure-results"
                                      overwrite="true"/>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <excludeDefaults>true</excludeDefaults>
        <plugins>
            <plugin>
                <groupId>ru.yandex.qatools.allure</groupId>
                <artifactId>allure-maven-plugin</artifactId>
                <version>2.5</version>
            </plugin>
        </plugins>
    </reporting>

</project>
